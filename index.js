/*
	ES6 Updates
*/

/*Exponents*/

/*Math.pow() to get the result of a number raised to a given exponent.*/
/*Math.pow(base,exponent)*/

/*let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);//125 = 5 x 5 x 5
*/

//Exponent Operators ** = allow us to get the result of a number raised to a given exponent
let fivePowerOf3 = 5**3;
console.log(fivePowerOf3);//125

/*
	Mini-Activity:

	Update the following codes to ES6 using the exponent operators.
	Log its values in the console.
	Debug any potential errors in the code.

*/

/*let fivePowerOf2 = Mathpow(5,2);
let fivePowerOf4 = Math.pow(4,5);
let lettwoPowerOf2 = Math.pow(2,2);*/
let fivePowerOf2 = 5**2;
let fivePowerOf4 = 5**4;
let twoPowerOf2 = 2**2;

console.log(fivePowerOf2);
console.log(fivePowerOf4);
console.log(twoPowerOf2);

/*
	Mini-Activity:

	Debug the following code that uses ES6 Exponent Operator:

	function squareRootChecker(number){
		console.log(num**.5);
	}

	let squareRootOf4 = squareRootChecker(4);
	console.log(squareRoot);
*/
//Solution
function squareRootChecker(num){
	return num**.5;
}

let squareRootOf4 = squareRootChecker(4);
console.log(squareRootOf4);

/*Template Literals*/
/*
	An Es6 Update to creating strings. Represented by backticks (``).

	'',"" - string literals

	`` - backticks/template literals


*/
let sampleString1 = `Charlie`;
let sampleString2 = `Joy`;
console.log(sampleString1);
console.log(sampleString2);
//Charlie and Joy into 1 string? "Charlie Joy"
/*let combinedString = `Charlie` + ` ` + `Joy`;*/
let combinedString = `${sampleString1} ${sampleString2}`;
/*
	We can create a single string to combine our variables. This is with the use of template literals(``) and by embedding JS expressions and variables in the string using ${} or placeholders.

*/
console.log(combinedString);

/*Template Literals with JS Expressions*/
let num1 = 15;
let num2 = 3;
//What if we want to say a sentence in the console like this:
//"The result of 15 plus 3 is 18"
let sum = num1+num2;
/*let sentence = "The result of " + num1 + " plus " + num2 + " is " + sum;*/
let sentence = `The result of ${num1} plus ${num2} is ${num1+num2}`
console.log(sentence);

/*
	Mini-Activity:

	Create a new variable called sentence2 which is able to show the result of 15 to the power of 2. In the console, the value of sentence2 should be:
	"The result of 15 to the power of 2 is <resultOf15ToPowerOf2>"

	Create a new variable called sentence3 which is able to show the result of 3 times 6. In the console, the value of sentence3 should be:
	"The result of 3 times 6 is <resultOf3Times6>"
	
*/
/*Mini Activity*/
let sentence2 = `The result of 15 to the power of 2 is ${num1**2}`;
let sentence3 = `The result of 3 times 6 is ${num2*6}`;
console.log(sentence2);
console.log(sentence3);

/*Array Destructuring*/
/*When we destructure an array, what we essentially do is to save array items in variables.*/
let array = ["Kobe","Lebron","Jordan"];

/*let goat1 = array[0];
console.log(goat1);
let goat2 = array[1];
let goat3 = array[2];

console.log(goat2,goat3);*/
/*
	Create variables called goat2 and goat3 and save the name, Lebron in goat2 and Jordan in goat3 by getting the items from the array.

*/
//Array Destructuring:
let [goat1,goat2,goat3] = array;
console.log(goat1);
console.log(goat2);
console.log(goat3);

let array2 = ["Curry","Lillard","Paul","Irving"];

/*
	Mini-Activity:

	Save the items in the array into the following variables:

	pg1 = Curry,
	pg2 = Lillard,
	pg3 = Paul,
	pg4 = Irving

	Log the values of each variable in the console.
*/
/*Mini-Activity*/
let [pg1,pg2,pg3,pg4] = array2;
console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);

/*
	In arrays, the order of items/elements is important and that goes the same to destructuring.
*/

let array3 = ["Jokic","Embiid","Anthony-Towns","Gobert"];
/*
	The order of variables where want to save our array items is also important.

	How can we skip an item?
	You can skip an item by adding another separator (,) and by not providing a variable to save the item we want to skip.

*/
let [center1,,center3] = array3;
console.log(center1);
console.log(center3);

let array4 = ["Draco Malfoy","Hermione Granger","Harry Potter","Ron Weasley","Professor Snape"]

/*
	Mini Activity

	Save the items in the array into the following variables:

	gryffindor1 = "Hermione Granger",
	gryffindor2 = "Harry Potter",
	gryffindor3 = "Ron Weasley",

	Log the values of the variables in the console.

*/
/*Mini-Activity*/
let [,gryffindor1,gryffindor2,gryffindor3] = array4;

console.log(gryffindor1);
console.log(gryffindor2);
console.log(gryffindor3);

/*
	Destrucutring can allow us to destructure an array and save items in variables and also into constants.

	let/const [variable1,variable2] = array;
*/

const [slytherin1,,,,slytherin2] = array4;
console.log(slytherin1);
console.log(slytherin2);

//variables can be updated.
gryffindor1 = "Emma Watson";
console.log(gryffindor1);

//constants cannot be updated.
/*slytherin1 = "Tom Felton";
console.log(slytherin1);
*/

/*Object Destructuring*/
/*It will allow us to destructure an object by saving/add the values of an object's property into respective variables.*/

let pokemon = {
	name: "Blastoise",
	level: 40,
	health: 80
}

/*
	Mini-Activity:

	Create 3 new sentence variables which will contain the following strings:

	sentence4 = The pokemon's name is <pokemonName>.
	sentence5 = It is a level <pokemonLevel> pokemon.
	sentence6 = It has at least <pokemonHealth> health points.

	Log the value of the variables in the console.

	Don't type in the details for the name,level and health of the pokemon. Use dot notation to access it instead.

*/
/*Mini-Activity*/
let sentence4 = `The pokemon's name is ${pokemon.name}.`;
let sentence5 = `It is a level ${pokemon.level} pokemon.`;
let sentence6 = `It has at least ${pokemon.health} health points.`
console.log(sentence4);
console.log(sentence5);
console.log(sentence6);

//We can save the values of an object's properties into variables.
//BY Object Destructuring:
/*
	Array Destructuring, order was important and the name of the variable we save our items in is not important.

	Object destructuring, order is not important however the name of the variables should match the properties of the object. Else, it may result to undefined.

*/
let {health,name,level,trainer} = pokemon;
console.log(health);
console.log(name);
console.log(level);
console.log(trainer);//undefined because there is no trainer property in our pokemon object.

let person = {
	name: "Paul Phoenix",
	age: 31,
	birthday: "January 12, 1991"
}

/*
	Mini-Activity

	Create a function named greet() which is able to receive an object as an argument. It should be able to destructure the object in the function and display the following messages in the console:

	"Hi! My name is <nameOfPerson>."
	"I am <ageOfPerson> years old."
	"My birthday is on <birthdayOfPerson>."

*/
/*Mini Activity*/
//function greet({name,age,birthday}){

	//destructure your object in the function
/*	let {name,age,birthday} = object;*/

	//console.log(`Hi! My name is ${name}.`);
	//console.log(`I am ${age} years old.`);
	//console.log(`My birthday is on ${birthday}`);

//}

//greet(person);

/*
	Mini-Activity:

	Debug the following codes when destructuring an array and an object.
*/

let actors = ["Tom Hanks","Leonardo DiCarpio","Anthony Hopkins","Ryan Reynolds"];

let director = {
	name: "Alfred Hitchcock",
	birthday: "August 13, 1889",
	isActive: false,
	movies: ["The Birds","Psycho","North by Northwest"]
};

let [actor1,actor2,actor3] = actors;

function displayDirector({name,birthday,movies}){

	/*let {name,birthday,movies} = person;*/

	console.log(`The Director's name is ${name}`);
	console.log(`He was born on ${birthday}`);
	console.log(`His Movies include:`);
	console.log(movies);

}

console.log(actor1);
console.log(actor2);
console.log(actor3);

displayDirector(director);

/*Arrow Functions*/
	
	//Arrow Functions are an alternative of writing/declaring functions. However, there are significant difference between our regular/traditional function and arrow functions.

	//regular/traditional function
	function displayMsg(){
		console.log(`Hello, World!`)
	}

	displayMsg();

	//arrow functions
	const hello = () => {
		console.log(`Hello from Arrow!`)
	}

	hello();

/*	function greet(personParams){

		console.log(`Hi, ${personParams.name}!`);

	};
*/
	//Mini-Activity Update the traditional function greet into an arrow function.
	//Note: Comment out the traditional function greet() and re-create using an arrow function

	const greet = (personParams) => {

		console.log(`Hi, ${personParams.name}!`)

	}

	greet(person);

	//anonymous functions in array methods

	//It allows us to iterate/loop over all items in an array.
	//An anonymous function is added so we can perform tasks per item in the array.
	//This anonymous function receives the current item being iterated/looped

/*	actors.forEach(function(actor){

		console.log(actor);

	})
*/	
	//Note: traditional functions cannot work without the curly braces.
	//Note: However, arrow functions can BUT it has to be a one-liner
	actors.forEach(actor=>console.log(actor));

	//.map() is similar to forEach wherein we can iterate over all items in our array.
	//The difference is we can return items from a map and create a new array out of it.

	let numberArr = [1,2,3,4,5];

/*	let multipliedBy5 = numberArr.map(function(number){

		return number*5

	})*/

	//Mini-Activity: recreate/update the anonymous function in the map as an arrow function.

	let multipliedBy5 = numberArr.map(number=>number*5)

	//arrow functions do not need the return keyword to return a value. This is called implicit return.
	//When arrow functions have a curly brace, it will need to have a return keyword to return a value. 

	console.log(multipliedBy5);

	/*Implicit Return for Arrow Functions*/

/*	function addNum(num1,num2){

		return num1+num2;

	};

	let sum1 = addNum(5,10);
	console.log(sum1);*/

	const subtractNum = (num1,num2) => num1 - num2;
	let difference1 = subtractNum(45,15);
	console.log(difference1);
	//Even without a return keyword, arrow functions can return a value. So long as its code block is not wrapped with {}

	/*
		Mini-Activity:

		Translate the traditional function addNum() into an arrow function with implicit return.

	*/

	/*Updated addNum()*/
	const addNum = (num1,num2) => num1+num2; 
	let sumExample = addNum(50,10);
	console.log(sumExample)

	/*this keyword in a method*/

	let protagonist = {
		name: "Cloud Strife",
		occupation: "SOLDIER",
		greet: function(){
			console.log(this);
			//this refers to the object where the method is in for methods created as traditional functions.
			console.log(`Hi! I am ${this.name}`);
		},
		introduceJob: () => {
			//this keyword in an arrow function does not refer to parent object, instead it refers to the global window object or the whole page.
			console.log(this);
			console.log(`I work as a ${this.occupation}`)
		}
	}

	protagonist.greet();//this refers to parent object.
	protagonist.introduceJob();//this.occupation results to undefined because this in a arrow function refers to the global window object.
	
	//Class-Based Objects Blueprints
		//In Javascript, Classes are templates/blueprints to creating objects.
		//We can create objects out of the use of Classes.
		//Before the introduction of Classes in JS, we mimic this behavior to create objects out of templates with the use of constructor functions.

		//Constructor function
			function Pokemon(name,type,level){

				this.name = name;
				this.type = type;
				this.level = level;

			}

			let pokemon1 = new Pokemon("Sandshrew","Ground",15);
			console.log(pokemon1);

		//Es6 Class Creation
			//We have a more distinctive way of creating classes instead of just our constructor functions

w

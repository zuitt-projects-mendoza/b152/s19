/*

	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
	2. Create a class constructor called Movie able to receive 5 arguments
		-Using the this keyword assign properties:
			title, //string
			director, //string
			producer, //string
			year, //num
			cast, //array 
			-assign the parameters as values to each property.

	Create 2 new objects using our class constructor.

	Log the 2 new Movie objects in the console.
	
*/

/*Debug and Update to Es6*/

let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401","Natural Sciences 402"]
}

	function introduce({name,age,classes}){

		console.log(`Hi! I'm ${name}. I am ${age} years old.`);
		console.log(`I study the following courses: ${classes}`);

	}

introduce(student1);
introduce(student2);



function getCube(num){

	console.log(num**3);

}

getCube(10);



let numArr = [15,16,32,21,21,2]

let [num1,num2,num3] = numArr;

console.log(num1);
console.log(num2);
console.log(num3);

numArr.forEach(num=>console.log(num));

let numsSquared = numArr.map(num=>num**2)

console.log(numsSquared)

/*2. Class Constructor*/

			class Movie {
				constructor(title,director,producer,year,cast){
					this.title = title;
					this.director = director;
					this.producer = producer;
					this.year = year;
					this.cast = cast
				}
			}

			let movie1 = new Movie("Harry Potter and the Sorcerer's Stone","Chris Columbus","David Heyman",2001,["Daniel Radcliffe","Emma Watson","Rupert Grint"]);

			let movie2 = new Movie("Spider-Man","Sam Raimi","Laura Ziskin",2002,["Tobey Maguire","Kirsten Dunst","Willem Dafoe"]);

			console.log(movie1);
			console.log(movie2);
